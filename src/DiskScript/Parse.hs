-- Copyright (C) 2020  Duncan Huntsinger

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

{-# LANGUAGE FlexibleContexts #-}
module DiskScript.Parse where

import DiskScript
import DiskScript.Runtime

import Data.Char

import Text.Parsec
import Text.Parsec.Token

diskScriptLanguage :: (Stream s m Char) => GenLanguageDef s u m
diskScriptLanguage = LanguageDef { commentStart = ""
                                 , commentEnd = ""
                                 , commentLine = "#"
                                 , nestedComments = False
                                 , identStart = letter <|> char '_'
                                 , identLetter = alphaNum <|> char '_'
                                 , opStart = oneOf "<-"
                                 , opLetter = oneOf ">"
                                 , caseSensitive = True
                                 , reservedNames = ["layout", "where", "use", "filesys", "mod", "extern", "put", "in", "begin", "end"]
                                 , reservedOpNames = ["<", "->"]
                                 }

lexer :: (Stream s m Char) => GenTokenParser s u m
lexer = makeTokenParser diskScriptLanguage

variable :: (Stream s m Char) => ParsecT s u m [Char]
variable = char '$' *> ((:) <$> (try $ letter <|> char '_') <*> (many $ try $ alphaNum <|> char '_'))

concatPath :: Path -> Path -> Path
concatPath End p2 = p2
concatPath p1 End = p1
concatPath (Basic p1 r1) (Basic p2 r2) = Basic (p1 ++ p2) $ concatPath r1 r2
concatPath (Basic p1 r1) p2 = Basic p1 $ concatPath r1 p2
concatPath (Interpolation v1 r1) p2 = Interpolation v1 $ concatPath r1 p2

basicPath :: (Stream s m Char) => ParsecT s u m Path
basicPath = Basic <$>
            ((++) <$>
              (concat <$>
                many ((try $ (++) <$>
                        (option "" (string "/")) <*>
                        (many1 $ noneOf " \t\n\r$()"))
                     )) <*>
              (option "" (string "/"))) <*>
            pure End

interpolatedPath :: (Stream s m Char) => ParsecT s u m Path
interpolatedPath = Interpolation <$> variable <*> pure End

path :: (Stream s m Char) => ParsecT s u m Path
path = (concatPath <$> (try interpolatedPath <|> try basicPath) <*> ((try $ satisfy $ not . isSpace) *> path <|> return End))

typeAnnotation :: (Stream s m Char) => ParsecT s u m Type
typeAnnotation = (try (symbol lexer "-d") *> pure Dir <|>
                  try (symbol lexer "-f") *> pure File <|>
                  try (symbol lexer "-m") *> pure Mount)

extern :: (Stream s m Char) => ParsecT s u m Extern
extern = (Extern <$> (lexeme lexer typeAnnotation) <*> (lexeme lexer variable) <*> (lexeme lexer basicPath))

useStatement :: (Stream s m Char) => ParsecT s u m Use
useStatement = reserved lexer "use" *> ((try (reserved lexer "filesys") *> (UseFilesys <$> (identifier lexer))) <|>
                                        (try (reserved lexer "mod")) *> (UseMod <$> (parens lexer $ many $ lexeme lexer (string "EFI" <|> string "GRUB"))) <|>
                                        (try (reserved lexer "extern")) *> (UseExtern <$> (parens lexer $ many1 $ lexeme lexer $ parens lexer extern)))

command :: (Stream s m Char) => ParsecT s u m Command
command = (try link) <|> (try dataflow) <|> (try declare)
  where
    link = Link <$> (path <* ((many $ char ' ') *> (string "->") *> (many $ char ' '))) <*> path
    dataflow = Dataflow <$> (path <* ((many $ char ' ') *> (string "<") *> (many $ char ' '))) <*> path
    declare = Declare <$> (typeAnnotation <* (many $ char ' ')) <*> path

layout :: (Stream s m Char) => ParsecT s u m Layout
layout = Layout <$>
         (reserved lexer "layout" *> whiteSpace lexer *> identifier lexer <* reserved lexer "where" <* whiteSpace lexer) <*>
         (useStatement `endBy` (char ';' <* whiteSpace lexer)) <*>
         ((char '{') *> (whiteSpace lexer) *> (command `endBy` whiteSpace lexer) <* (char '}'))

locator :: (Stream s m Char) => ParsecT s u m Locator
locator = Put <$> (reserved lexer "put" *> whiteSpace lexer *> identifier lexer) <*>
          (fromInteger <$> (reserved lexer "in" *> whiteSpace lexer *> char '@' *> integer lexer))

program :: (Stream s m Char) => ParsecT s u m Program
program = Program <$> (whiteSpace lexer *> (many $ lexeme lexer layout)) <*> ((locator `endBy` (char ';' *> whiteSpace lexer)))

readProgram :: FilePath -> IO (Either ParseError Program)
readProgram inputPath = parse program inputPath <$> readFile inputPath
